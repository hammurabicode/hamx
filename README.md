# hammurabi X README

[![DOI](https://zenodo.org/badge/238854871.svg)](https://zenodo.org/badge/latestdoi/238854871)

[![Build Status](https://travis-ci.org/hammurabi-dev/hammurabiX.svg?branch=master)](https://travis-ci.org/hammurabi-dev/hammurabiX)

hammurabi is an open-source (GNU General Public License v3) HEALPix-based tool 
for simulating full/partial-sky Galactic emissions.
The outputs of (current version) hammurabi X include:

- **polarized synchrotron emission**
- **dispersion measure** 
- **Faraday depth**

Essential physical inputs/modelings required during simulation include:  

- **Galactic magnetic field**
- **(cosmic-ray & thermal) electron distribution**

hammurabi X is a modular C++ framework which is friendly to user defined models.
Based on earlier versions of hammurabi, we mainly focus on improving its numerical reliablility and scalability.

Please check our [**WIKI PAGE**](https://bitbucket.org/hammurabicode/hamx/wiki/Home) for more detailed technical information.

The original hammurabi source code can be found [**here**](https://sourceforge.net/projects/hammurabicode/).

### hammurabi team publications:

- [hammurabi X: Simulating Galactic Synchrotron Emission with Random Magnetic Fields](https://arxiv.org/abs/1907.00207)

- [Simulating polarized Galactic synchrotron emission at all frequencies. The Hammurabi code](https://www.aanda.org/articles/aa/abs/2009/08/aa10564-08/aa10564-08.html)

### contact
*bug reports and code contributions are warmly welcomed, feel free to contact*

- [Jiaxin Wang](https://gioacchinowang.bitbucket.io/)
- [Tess Jaffe](https://science.gsfc.nasa.gov/sed/bio/tess.jaffe)
- [Torsten Ensslin](https://wwwmpa.mpa-garching.mpg.de/~ensslin/)
